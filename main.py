import pygame

white = pygame.color.Color(255, 255, 255)
black = pygame.color.Color(0, 0, 0)

width = 1600
height = 740
border = 30
velocity = 3
background = black
foreground = white


gameDisplay = pygame.display.set_mode((width, height))

class Ball:
	RADIUS = 20
	def __init__(self, x, y, vx, vy):
		self.x = x
		self.y = y
		self.vx = vx
		self.vy = vy

	def show(self):
		global gameDisplay
		pygame.draw.circle(gameDisplay, foreground, (self.x, self.y), self.RADIUS)
	
	def update(self):
		self.delete()

		if self.y - self.RADIUS <= border or self.y + self.RADIUS >= height - border:
			self.vy = -self.vy
		elif self.x - self.RADIUS <= border:
			self.vx = -self.vx
		elif self.x + self.RADIUS >= width - 2 * userInGame.width and self.y < userInGame.y + userInGame.height//2 and self.y > userInGame.y - userInGame.height//2:
			self.vx = -self.vx

		self.x = self.x + self.vx
		self.y = self.y + self.vy
		self.show()
	
	def delete(self):
		pygame.draw.circle(gameDisplay, background, (self.x, self.y), self.RADIUS)

class User:

	width = 20
	height = 120

	def __init__(self, y):
		self.y = y

	def show(self):
		global gameDisplay
		pygame.draw.rect(gameDisplay, foreground, pygame.Rect((width - 2 * self.width, self.y - self.height//2), (self.width, self.height)))

	def delete(self):
		global gameDisplay
		pygame.draw.rect(gameDisplay, background, pygame.Rect((width - 2 * self.width, self.y - self.height//2), (self.width, self.height)))

	def update(self):
		self.delete()
		self.y = pygame.mouse.get_pos()[1]
		self.show()


ballInGame = Ball(width - 5 * Ball.RADIUS, height//2, -velocity, -velocity)
userInGame = User(height//2 - 20)


while(True):
	e = pygame.event.poll()
	if e.type == pygame.QUIT:
		break


	up = pygame.draw.rect(gameDisplay, foreground, pygame.Rect((0,0), (width, border)))
	left = pygame.draw.rect(gameDisplay, foreground, pygame.Rect((0, border), (border, height - border)))
	down = pygame.draw.rect(gameDisplay, foreground, pygame.Rect((border, height - border), (width - border, border)))

	ballInGame.update()
	userInGame.update()
	

	if ballInGame.x > width:
		ballInGame = Ball(width - 5 * Ball.RADIUS, height//2, -velocity, -velocity)

	pygame.display.flip()